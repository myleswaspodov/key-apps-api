
const bcrypt = require('bcryptjs');
const core   = require('../src/core');

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        {
          id           : 1, 
          username     : 'waspodov',
          email        : 'dwiheru18@gmail.com',
          email_verify : 'GANTENG',
          phone        : '083847090002',
          password     : bcrypt.hashSync('ganteng', core.saltRounds),
          code         : "ganteng",
          created_at   : new Date().toISOString(),
          updated_at   : new Date().toISOString()
        }
      ]);
    });
};
