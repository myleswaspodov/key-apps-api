import jwt from "jsonwebtoken";
import core from "../core";

export default function auth() {
	return async (req, res, next) => {
		let token = req.headers

		try {
			var decoded = jwt.verify(token.authorization, core.jwtToken);
			req.user = decoded
			return next()
		} catch(err) {
		  	return res.json({error: "Invalid token"})
		}
	}
}