'use strict'
import Promise from 'bluebird';
import core from "../../core";
import { KeyPassModel } from "./KeyPassModel";
import { validateCreate, validateUpdate, validateDelete } from "./KeyPassValidation";

const bookshelf = core.database
const returnApi = core.returnApi

export const create = async (req, res) => {
	let data       = req.body
	let validation = await validateCreate(data)

	if (validation.fails()) {
		return res.json(validation.errors.all())
	}

	Object.assign(data, {id_user: req.user.id})

	let save = await bookshelf.default.transaction(function (transaction) {
				KeyPassModel.forge()
					.save(data, {transacting: transaction})
				    .then(transaction.commit)
				    .catch(transaction.rollback);
				}).asCallback();

	return res.json(returnApi(save))
}

export const update = async (req, res) => {
	let data       = req.body
	let validation = await validateUpdate(data)

	if (validation.fails()) {
		return res.json(validation.errors.all())
	}

	let dataUpdate = {}

	if (data.key) {
		Object.assign(dataUpdate, {key: data.key})
	}
	if (data.key) {
		Object.assign(dataUpdate, {username: data.username})
	}
	if (data.key) {
		Object.assign(dataUpdate, {password: data.password})
	}

	if (!(!Object.keys(dataUpdate).length)) {
		let save = await bookshelf.default.transaction(function (transaction) {
				KeyPassModel.forge()
					.where({id: data.id})
					.save(dataUpdate, {transacting: transaction})
				    .then(transaction.commit)
				    .catch(transaction.rollback);
				}).asCallback();

		return res.json(returnApi(save))
	}
	
	return res.json(returnApi({}))
}

export const deleted = async (req, res) => {
	let data       = req.body
	let validation = await validateDelete(data)

	if (validation.fails()) {
		return res.json(validation.errors.all())
	}

	let deleted = await bookshelf.default.transaction(function (transaction) {
			KeyPassModel.forge({id: data.id})
				.destroy()
			    .then(transaction.commit)
			    .catch(KeyPassModel.NoRowsDeletedError, function() {
			        transaction.rollback
			        return res.json(returnApi(false))
			    })
			    .catch(function(err) {
			    	transaction.rollback
			        return res.json(err)
				})
			}).asCallback();

	return res.json(returnApi(true))
}