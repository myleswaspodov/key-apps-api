import core from '../../core';
import {UserModel} from "../Users/UserModel";

const bookshelf = core.database;

export const KeyPassModel = bookshelf.default.Model.extend({
	tableName     :'user_keypasswords',
	idAttribute   :'id',
	hasTimestamps : true,
	user       : function() {
        return this.belongsTo(UserModel, 'id')
    }
});