import core from '../../core';
import {KeyPassModel} from "../Keypass/KeyPassModel";

const bookshelf = core.database.default;
bookshelf.plugin('visibility')

export const UserModel = bookshelf.Model.extend({
	tableName     :'users',
	idAttribute   :'id',
	hasTimestamps : true,
	// hidden        : ['password'],
	keyPass       : function() {
        return this.hasMany(KeyPassModel, 'id_user')
    }
});