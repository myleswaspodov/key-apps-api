'use strict'

/* RETURN API*/
const returnApi = (data) => {
  // console.log(typeof data)
  // console.log(data.rows)
    var result = {}

    if (data !== undefined) {

      if (typeof data === "object") {
        if (!(!Object.keys(data).length)) {
          // if (data.rows.length > 0) {
            Object.assign(result, {status: "success", data: data})
          // }
        } else {
          Object.assign(result, {status: "fail"})
        }
      } else {
        if (data === true || data === 1) {
          Object.assign(result, {status: "success"})
        } else {
          Object.assign(result, {status: "fail"})
        }
      }
    } else {
      Object.assign(result, {status: "fail"})
    }

    return result
}

/* RANDOM CODE */
const createRandom = (digit) => {
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
    let text     = ""
    let start    = 0

    while (start < digit) {
      var temp = possible.charAt(Math.floor(Math.random() * possible.length));
      text     += temp
      start++
      possible = possible.replace(temp, "")
    }

    return text;
}

const base64encode = (str) => {
  return Buffer.from(str).toString('base64')
}

const base64decode = (str) => {
  return Buffer.from(str, 'base64').toString()
}
module.exports = { returnApi, createRandom, base64encode, base64decode }