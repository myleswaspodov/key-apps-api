// import database from './database';
// import { returnApi, createRandom, base64encode, base64decode } from './myhelper';
const database     = require('./database')
const { returnApi, createRandom, base64encode, base64decode } = require('./myhelper')
const { sendEmail } = require('./mail')

// for password hash
const saltRounds  = 10
const jwtToken    = "token"
const digitVerify = 6
const digitCode   = 4

// export default { database, returnApi, createRandom, base64encode, base64decode, saltRounds, jwtToken, sendEmail, digitVerify, digitCode }
module.exports = { database, returnApi, createRandom, base64encode, base64decode, saltRounds, jwtToken, sendEmail, digitVerify, digitCode }