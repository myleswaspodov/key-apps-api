import nodemailer from 'nodemailer';
import configs from '../../configs';

export const sendEmail = async (to, subject, message) => {
	let transporter = nodemailer.createTransport(configs.email);

	// setup email data with unicode symbols
	let mailOptions = {
		from    : '"Keypass apps 👻" <key@waspodov.com>',
		to      : to,
		subject : '✔ ' + subject, 
		html    : message
	}

	let sendMail = await transporter.sendMail(mailOptions).then(function(info){
	    return info
	}).catch(function(err){
		return err
	});

	return sendMail
}