
exports.up = function(knex, Promise) {
	return knex.schema.createTableIfNotExists('users', table => {
		table.increments().primary()
		table.string('username', 80).notNullable()
		table.string('email', 200).notNullable().unique()
		table.string('email_verify', 20).nullable()
		table.string('phone', 20).notNullable().unique().nullable()
		table.string('password', 255).notNullable()
		table.string('code', 60).notNullable()
		table.timestamps()
	});
};

exports.down = function(knex, Promise) {
  	return knex.schema.dropTable('users');
};
