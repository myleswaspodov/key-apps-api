
exports.up = function(knex, Promise) {
  	return knex.schema.createTableIfNotExists('user_keypasswords', table => {
		table.increments().primary()
		table.integer('id_user').unsigned().references('id').inTable('users').onDelete('set null')
		table.string('key', 100).notNullable()
		table.string('username', 100).notNullable()
		table.string('password', 255).notNullable()
		table.timestamps()
	});
};

exports.down = function(knex, Promise) {
  	return knex.schema.dropTable('user_keypasswords');
};
